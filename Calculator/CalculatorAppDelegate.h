//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Meng-Gen Tsai on 1/18/13.
//  Copyright (c) 2013 Meng-Gen Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
