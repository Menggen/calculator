//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Meng-Gen Tsai on 1/18/13.
//  Copyright (c) 2013 Meng-Gen Tsai. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()

@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;

@property (nonatomic, strong) CalculatorBrain *brain;

- (void)appendSentToBrainStringToBrainDisplay:(NSString *)sentString;

@end



@implementation CalculatorViewController

@synthesize display = _display;

@synthesize brainDisplay = _brainDisplay;

@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;

@synthesize brain = _brain;

- (CalculatorBrain *)brain
{
    if (!_brain) {
        _brain = [[CalculatorBrain alloc] init];
    }
    return _brain;
}

- (IBAction)digitPressed:(UIButton *)sender
{
    NSString *digit = sender.currentTitle;
    NSLog(@"digit pressed = %@", digit);
    if (self.userIsInTheMiddleOfEnteringANumber) {
        self.display.text = [self.display.text stringByAppendingString:digit];
    } else {
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
}

- (IBAction)decimalPointPressed:(UIButton *)sender {
    NSLog(@"decimal point pressed");
    
    NSRange decimalPointRange = [self.display.text rangeOfString:@"."];
    if (decimalPointRange.location == NSNotFound) {
        if (self.userIsInTheMiddleOfEnteringANumber) {
            self.display.text = [self.display.text stringByAppendingString:@"."];
        } else {
            self.display.text = @".";
            self.userIsInTheMiddleOfEnteringANumber = YES;
        }
    }
}

- (IBAction)clearPressed {
    [self.brain resetBrain];
    self.display.text = @"0";
    self.brainDisplay.text = @"";
    self.userIsInTheMiddleOfEnteringANumber = NO;
}

- (IBAction)changeSignPressed {
}

- (IBAction)operationPressed:(UIButton *)sender
{
    if (self.userIsInTheMiddleOfEnteringANumber) {
        [self enterPressed];
    }
    self.display.text = [NSString stringWithFormat:@"%g", [self.brain performOperation:sender.currentTitle]];
    [self appendSentToBrainStringToBrainDisplay:sender.currentTitle];
}

- (IBAction)enterPressed
{
    [self.brain pushOperand:[self.display.text doubleValue]];
    [self appendSentToBrainStringToBrainDisplay:self.display.text];
    self.userIsInTheMiddleOfEnteringANumber = NO;
}

- (void)appendSentToBrainStringToBrainDisplay:(NSString *)sentToBrainString
{
    if (![self.brainDisplay.text isEqualToString:@""]) {
        self.brainDisplay.text = [self.brainDisplay.text stringByAppendingString:@" "];
    } 
    self.brainDisplay.text = [self.brainDisplay.text stringByAppendingString:sentToBrainString];
}

@end
