//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Meng-Gen Tsai on 1/18/13.
//  Copyright (c) 2013 Meng-Gen Tsai. All rights reserved.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain ()
@property (nonatomic, strong) NSMutableArray *operandStack;
@property (nonatomic) BOOL hasConstantOperation;
@property (nonatomic) double constantOperationEvaluationValue;
@end

@implementation CalculatorBrain

@synthesize operandStack = _operandStack;
@synthesize hasConstantOperation = _hasConstantOperation;
@synthesize constantOperationEvaluationValue = _constantOperationEvaluationValue;

- (NSMutableArray *)operandStack
{
    if (!_operandStack) {
        _operandStack = [[NSMutableArray alloc] init];
    }
    return _operandStack;
}

- (void)pushOperand:(double)operand
{
    [self.operandStack addObject:[NSNumber numberWithDouble:operand]];
}

- (void)resetBrain {
    [self.operandStack removeAllObjects];
    self.hasConstantOperation = NO;
}

- (double)popOperand
{
    NSNumber *operandObject = [self.operandStack lastObject];
    
    if (operandObject) {
        [self.operandStack removeLastObject];
    }
    
    return [operandObject doubleValue];
}

- (double)performOperation:(NSString *)operation
{
    double result = 0;
    
    if ([operation isEqualToString:@"+"]) {
        double addend = [self popOperand];
        double summand = [self popOperand];
        if (!summand && self.hasConstantOperation) {
            summand = self.constantOperationEvaluationValue;
        }
        result = addend + summand;
    }
    else if ([operation isEqualToString:@"-"]) {
        double subtrahend = [self popOperand];
        double minuend = [self popOperand];
        if (!minuend && self.hasConstantOperation) {
            minuend = self.constantOperationEvaluationValue;
        }
        result = minuend - subtrahend;
    }
    else if ([operation isEqualToString:@"*"]) {
        double multiplier = [self popOperand];
        double mulitplicand = [self popOperand];
        if (!mulitplicand && self.hasConstantOperation) {
            mulitplicand = self.constantOperationEvaluationValue;
        }
        result = mulitplicand * multiplier;
    }
    else if ([operation isEqualToString:@"/"]) {
        double divisor = [self popOperand];
        double dividend = [self popOperand];
        if (!dividend && self.hasConstantOperation) {
            dividend = self.constantOperationEvaluationValue;
        }
        
        if (divisor) {
            result = dividend / divisor;
        } else {
            result = 0;
        }
    }
    else if ([operation isEqualToString:@"e"]) {
        self.hasConstantOperation = YES;
        self.constantOperationEvaluationValue = M_E;
        result = [self popOperand];
    }
    else if ([operation isEqualToString:@"π"]) {
        self.hasConstantOperation = YES;
        self.constantOperationEvaluationValue = M_PI;
        result = [self popOperand];
    }
    else if ([operation isEqualToString:@"+/-"]) {
        result = -[self popOperand];
    }
    else if ([operation isEqualToString:@"sin"]) {
        result = sin([self popOperand]);
    }
    else if ([operation isEqualToString:@"cos"]) {
        result = cos([self popOperand]);
    }
    else if ([operation isEqualToString:@"sqrt"]) {
        double number = [self popOperand];
        if (number >= 0) {
            result = sqrt(number);
        } else {
            result = 0;
        }
    }
    else if ([operation isEqualToString:@"log"]) {
        double number = [self popOperand];
        if (number > 0) {
            result = log(number);
        } else {
            result = 0;
        }
    }
    
    [self pushOperand:result];
    
    return result;
}

@end
