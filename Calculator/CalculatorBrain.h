//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Meng-Gen Tsai on 1/18/13.
//  Copyright (c) 2013 Meng-Gen Tsai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

- (void)pushOperand:(double)operand;

- (void)resetBrain;

- (double)performOperation:(NSString *)operation;

@end
